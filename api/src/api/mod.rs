use gotham::state::State;
use hyper::Body;
use hyper::Response;

pub mod assets;

pub fn terminate(_state: State) -> (State, Response<Body>) {
    std::process::exit(0);
}
