use gotham::middleware::session::NewSessionMiddleware;
use gotham::pipeline::new_pipeline;
use gotham::pipeline::single::single_pipeline;
use gotham::router::builder::build_router;
use gotham::router::builder::DefineSingleRoute;
use gotham::router::builder::DrawRoutes;
use serde::{Deserialize, Serialize};

mod api;

use api::assets::*;

#[derive(Default, Serialize, Deserialize)]
struct SessionType();

pub fn main() {
    let addr = "127.0.0.1:8080";

    let (chain, pipelines) = single_pipeline(
        new_pipeline()
            .add(NewSessionMiddleware::default().with_session_type::<SessionType>())
            .build(),
    );

    let router = build_router(chain, pipelines, |route| {
        route
            .get("api/v1/assets/*")
            .with_path_extractor::<PathExtractor>()
            .to(get_asset);
    });

    gotham::start(addr, router);
}
