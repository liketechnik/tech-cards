pub enum Window {
    Overview,
    About,
}

pub struct Model {
    pub window: Window,
    pub readme: String,
}

impl Default for Model {
    fn default() -> Self {
        Self {
            window: Window::Overview,
            readme: "Default Text".to_owned(),
        }
    }
}
