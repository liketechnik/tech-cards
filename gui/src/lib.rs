extern crate seed;

pub use seed::{prelude::*, *};

pub mod model;
pub use model::*;

pub mod update;
pub use update::Msg;

pub mod view;

pub mod commands;
pub use commands::*;

fn window_events(_model: &Model) -> Vec<EventHandler<Msg>> {
    vec![ev(Ev::BeforeUnload, Msg::OnClose)]
}

fn after_mount(_: Url, orders: &mut impl Orders<Msg>) -> AfterMount<Model> {
    orders.perform_cmd(fetch_readme());
    AfterMount::default()
}

fn routes(url: Url) -> Option<Msg> {
    match url.hash().map(String::as_ref) {
        Some("about") => Some(Msg::ChangeWindow(Window::About)),
        Some("overview") => Some(Msg::ChangeWindow(Window::Overview)),
        _ => Some(Msg::ChangeWindow(Window::Overview)),
    }
}

#[wasm_bindgen(start)]
pub fn render() {
    App::builder(update::update, view::view)
        .after_mount(after_mount)
        .window_events(window_events)
        .routes(routes)
        .build_and_start();
}
