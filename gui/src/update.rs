use super::*;

use seed::{prelude::*, *};

pub enum Msg {
    Error(String),
    FetchedReadme(String),
    ChangeWindow(Window),
    OnClose(web_sys::Event),
}

pub fn update(msg: Msg, model: &mut Model, orders: &mut impl Orders<Msg>) {
    match msg {
        Msg::Error(err) => {
            let _res = window().alert_with_message(&format!("There was an error: {}", err));
        }
        Msg::FetchedReadme(content) => {
            model.readme = content;
            orders.skip();
        }
        Msg::OnClose(_event) => {
            log!("closing");
            // TODO implement order
        }
        Msg::ChangeWindow(window) => model.window = window,
    }
}
