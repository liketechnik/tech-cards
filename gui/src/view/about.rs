use super::*;

pub fn view_about(model: &Model) -> Vec<Node<Msg>> {
    vec![div![attrs! {At::Class => "col"}, md!(&model.readme)]]
}
