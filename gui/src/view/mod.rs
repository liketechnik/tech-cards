use super::*;

pub mod about;
pub use about::*;

pub fn view(model: &Model) -> Node<Msg> {
    div![
        header![
            attrs! {At::Class => "navbar"},
            section![
                attrs! {At::Class => "navbar-section"},
                a![
                    "Techcards - Flashcard manager",
                    attrs! {At::Href => "./index.html#overview", At::Class => "navbar-brand mr-2 text-bold text-large"}
                ],
                a![
                    "About",
                    attrs! {At::Href => "./index.html#about", At::Class => "btn btn-link"}
                ],
            ],
        ],
        match model.window {
            Window::Overview => vec![p!["hello"]],
            Window::About => view_about(model),
        },
    ]
}
