use seed::{prelude::*, *};

use super::*;

pub async fn fetch_readme() -> Msg {
    let request = Request::new("/api/v1/assets/README.md").method(Method::Get);

    match fetch(request).await {
        Ok(response) => {
            if response.status().is_ok() {
                match response.text().await {
                    Ok(text) => Msg::FetchedReadme(text),
                    Err(err) => Msg::Error(format!("{:#?}", err)),
                }
            } else {
                Msg::Error(response.status().text)
            }
        }
        Err(err) => Msg::Error(format!("{:#?}", err)),
    }
}
