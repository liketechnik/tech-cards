use crate::entity::techcard::Techcard;
use crate::entity::unit::Unit;
use crate::entity::EntityError;
use std::path::PathBuf;

pub fn parse_dir<U, C>(dir: PathBuf, mut add_unit: U, mut add_card: C) -> Result<(), EntityError>
where
    U: FnMut(Unit) -> (),
    C: FnMut(Techcard) -> (),
{
    let root = Unit::parse_dir(dir)?;
    let path = root.path().to_owned();
    add_unit(root);

    for entry in path.read_dir().map_err(|e| EntityError::DirRead {
        path: path.to_path_buf(),
        source: e,
    })? {
        let entry = entry
            .map_err(|e| EntityError::DirRead {
                path: path.to_path_buf(),
                source: e,
            })?
            .path();

        if entry.is_dir() {
            parse_dir_rec(entry.to_owned(), &mut add_unit, &mut add_card)?;
        }
    }

    Ok(())
}

fn parse_dir_rec<U, C>(
    mut dir: PathBuf,
    add_unit: &mut U,
    add_card: &mut C,
) -> Result<(), EntityError>
where
    U: FnMut(Unit) -> (),
    C: FnMut(Techcard) -> (),
{
    dir.push("unit");
    if dir.exists() {
        dir.pop();
        let unit = Unit::parse_dir(dir.to_owned())?;
        let path = unit.path().to_owned();
        add_unit(unit);

        for entry in path.read_dir().map_err(|e| EntityError::DirRead {
            path: path.to_path_buf(),
            source: e,
        })? {
            let entry = entry
                .map_err(|e| EntityError::DirRead {
                    path: path.to_path_buf(),
                    source: e,
                })?
                .path();
            if entry.is_dir() {
                parse_dir_rec(entry.to_owned(), add_unit, add_card)?;
            }
        }
    } else {
        dir.pop();
    }

    dir.push("techcard");
    if dir.exists() {
        dir.pop();
        let card = Techcard::parse_dir(dir)?;
        add_card(card);
    } else {
        dir.pop();
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;
    use uuid::Uuid;

    #[test]
    fn read_into_vec() {
        let mut units: Vec<Unit> = Vec::new();
        let mut cards: Vec<Techcard> = Vec::new();

        parse_dir(
            PathBuf::from("tests/single_unit"),
            |u: Unit| (&mut units).push(u),
            |c: Techcard| (&mut cards).push(c),
        )
        .unwrap();

        println!("Units: {:?}", units);
        println!("Cards: {:?}", cards);
    }

    #[test]
    fn read_into_map() {
        let mut units: HashMap<Uuid, Unit> = HashMap::new();
        let mut cards: HashMap<Uuid, Techcard> = HashMap::new();

        parse_dir(
            PathBuf::from("tests/single_unit"),
            |u: Unit| {
                (&mut units).insert(u.uuid().to_owned(), u);
            },
            |c: Techcard| {
                (&mut cards).insert(c.uuid().to_owned(), c);
            },
        )
        .unwrap();

        println!("Units: {:?}", units);
        println!("Cards: {:?}", cards);
    }
}
