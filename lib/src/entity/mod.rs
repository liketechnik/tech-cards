pub mod techcard;
pub use techcard::Techcard;
pub mod unit;
pub use unit::Unit;

use std::borrow::Cow;
use std::collections::HashMap;
use std::ffi::OsStr;
use std::ffi::OsString;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::Write;
use std::path::{Path, PathBuf};

use thiserror::Error;

#[derive(Error, Debug)]
pub enum EntityError {
    #[error("The name and directory must be equal. Got {} and {}", name, path.display())]
    NameMissmatch { name: String, path: PathBuf },
    #[error("Can't get name for {0}.")]
    Name(PathBuf),
    #[error("Failed to read directory: {}", path.display())]
    DirRead {
        path: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("Failed to read file into buffer: {}", path.display())]
    FileRead {
        path: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("Failed to write file: {}", path.display())]
    FileWrite {
        path: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("Failed to delete: {}", path.display())]
    DirDelete {
        path: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("Failed to create directory: {}", path.display())]
    DirCreate {
        path: PathBuf,
        #[source]
        source: std::io::Error,
    },
    #[error("Parent directory must exist to save at: {0}")]
    NoParentDir(PathBuf),
    #[error("{0} must be a directory")]
    NoDir(PathBuf),
    #[error("No {} file found at {}.", part.to_string_lossy(), path.display())]
    FileMissing { part: OsString, path: PathBuf },
    #[error(transparent)]
    Uuid(#[from] uuid::Error),
}

fn dir_name(dir: &Path) -> Result<String, EntityError> {
    let dirname = dir.file_name();
    match dirname {
        None => Err(EntityError::Name(dir.to_path_buf())),
        Some(dirname) => match dirname.to_string_lossy() {
            Cow::Owned(dirname) => Ok(dirname),
            Cow::Borrowed(dirname) => Ok(dirname.to_owned()),
        },
    }
}

fn read_files<'a>(
    dir: &Path,
    entries: &'a [&OsStr],
) -> Result<HashMap<&'a OsStr, String>, EntityError> {
    let mut files: HashMap<&'a OsStr, String> = HashMap::with_capacity(entries.len());

    for entry in dir.read_dir().map_err(|e| EntityError::DirRead {
        path: dir.to_path_buf(),
        source: e,
    })? {
        let entry = entry.map_err(|e| EntityError::DirRead {
            path: dir.to_path_buf(),
            source: e,
        })?;
        let file = entry.path();
        if file.is_file() {
            let name = file.file_name();
            match name {
                Some(name) => {
                    let entry = entries.iter().find(|&x| x == &name);
                    if let Some(entry) = entry {
                        let mut handle = File::open(&file).map_err(|e| EntityError::FileRead {
                            path: file.to_path_buf(),
                            source: e,
                        })?;
                        let mut content = String::new();

                        handle
                            .read_to_string(&mut content)
                            .map_err(|e| EntityError::FileRead {
                                path: file,
                                source: e,
                            })?;
                        files.insert(entry, content);
                    }
                }
                None => return Err(EntityError::Name(file)),
            }
        }
    }

    Ok(files)
}

fn save_files(dir: &mut PathBuf, files: HashMap<&OsStr, &[u8]>) -> Result<(), EntityError> {
    if !dir
        .parent()
        .ok_or_else(|| EntityError::NoParentDir(dir.to_path_buf()))?
        .is_dir()
    {
        return Err(EntityError::NoParentDir(dir.to_path_buf()));
    }

    if !dir.is_dir() {
        fs::create_dir(&dir).map_err(|e| EntityError::DirCreate {
            path: dir.to_owned(),
            source: e,
        })?;
    }

    for file in files {
        dir.push(file.0);
        let mut buffer = File::create(&dir).map_err(|e| EntityError::FileWrite {
            path: dir.to_owned(),
            source: e,
        })?;

        buffer
            .write_all(file.1)
            .map_err(|e| EntityError::FileWrite {
                path: dir.to_owned(),
                source: e,
            })?;

        dir.pop();
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_delete_cycle() {
        let upath = PathBuf::from("./generated/single_unit");
        let unit = Unit::new(
            "single_unit".to_owned(),
            "let's test it".to_owned(),
            upath.to_owned(),
        )
        .unwrap();
        let unit = unit.save().unwrap();

        let tpath = PathBuf::from("./generated/single_unit/simple_card");
        let techcard = Techcard::new(
            "simple_card".to_owned(),
            "question".to_owned(),
            "answer".to_owned(),
            tpath.to_owned(),
        )
        .unwrap();
        let techcard = techcard.save().unwrap();

        let unitl = Unit::parse_dir(upath).unwrap();
        assert_eq!(unit, unitl);

        let techcardl = Techcard::parse_dir(tpath).unwrap();
        assert_eq!(techcard, techcardl);

        unit.delete().unwrap();
    }
}
