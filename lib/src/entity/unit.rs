use serde::{Deserialize, Serialize};
use uuid::Uuid;

use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs;
use std::path::Path;
use std::path::PathBuf;

use super::EntityError;

/// A collection of [Techcards](super::Techcard).
/// The [uuid](Unit::uuid) field is the unique identifier.
/// The [name][Unit::name] is the directory where the unit is located.
/// The [content][Unit::content] are all submodules and cards.
/// The [path][Unit::path] is where the [Unit] is stored on the filesystem.
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Unit {
    pub(crate) uuid: Uuid,
    pub(crate) name: String,
    pub description: String,
    pub(crate) content: Vec<Uuid>,
    pub(crate) path: PathBuf,
}

impl Unit {
    pub fn uuid(&self) -> &Uuid {
        &self.uuid
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn content(&self) -> &[Uuid] {
        &self.content
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Parse a Unit's filesystem representation at `dir`.
    ///
    /// The [content][Unit::content] field is NOT populated,
    /// as there's no easy way to verify if sub-directories contain
    /// valid [Units][Unit] or [Techcards][super::Techcard].
    ///
    /// The passed [PathBuf] is expanded to its absolute path.
    pub fn parse_dir(dir: PathBuf) -> Result<Unit, EntityError> {
        if !dir.is_dir() {
            return Err(EntityError::NoDir(dir));
        }

        let name = super::dir_name(&dir)?;

        let uuidname = OsStr::new("unit");
        let descname = OsStr::new("description");
        let files = vec![uuidname, descname];

        let file_content = super::read_files(&dir, &files)?;

        let uuid = file_content.get(uuidname).ok_or(EntityError::FileMissing {
            part: uuidname.to_owned(),
            path: dir.to_owned(),
        })?;
        let description = file_content.get(descname).ok_or(EntityError::FileMissing {
            part: descname.to_owned(),
            path: dir.to_owned(),
        })?;

        Ok(Unit {
            uuid: Uuid::parse_str(uuid)?,
            name,
            description: description.to_string(),
            content: Vec::new(),
            path: dir,
        })
    }

    /// Recursively deletes a Unit's filesystem representation at `path`.
    ///
    /// Does not do any safety checks.
    pub fn delete(self) -> Result<(), (Self, EntityError)> {
        fs::remove_dir_all(&self.path)
            .map_err(|e| EntityError::DirDelete {
                path: self.path.to_owned(),
                source: e,
            })
            .map_err(|e| (self, e))?;
        Ok(())
    }

    /// Creates a new in-memory representation of a Unit.
    /// To save the representaiton call `save()`.
    ///
    /// Generates a new UUID for the Unit on invocation.
    pub fn new(name: String, description: String, dir: PathBuf) -> Result<Unit, EntityError> {
        if !dir.ends_with(&name) {
            return Err(EntityError::NameMissmatch { name, path: dir });
        }

        Ok(Unit {
            uuid: Uuid::new_v4(),
            name,
            description,
            content: Vec::new(),
            path: dir,
        })
    }

    /// Saves the Unit's content at `path`.
    ///
    /// If the directory for the unit does not
    /// exist, it is created.
    /// Note that the parent directory must already
    /// exist though.
    pub fn save(self) -> Result<Self, (Self, EntityError)> {
        let mut files: HashMap<&OsStr, &[u8]> = HashMap::new();
        let uuid = self.uuid.to_string();
        files.insert(OsStr::new("unit"), uuid.as_bytes());
        files.insert(OsStr::new("description"), self.description.as_bytes());

        let mut path = PathBuf::from(&self.path);

        match super::save_files(&mut path, files) {
            Ok(_) => Ok(self),
            Err(e) => Err((self, e)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read() {
        let result = Unit::parse_dir(PathBuf::from(r"./tests/single_unit"));
        match result {
            Ok(unit) => println!("{:?}", unit),
            Err(error) => {
                println!("Error: {}", error);
                panic!()
            }
        }
    }
}
