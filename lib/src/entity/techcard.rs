use serde::{Deserialize, Serialize};
use std::path::Path;
use uuid::Uuid;

use std::collections::HashMap;
use std::ffi::OsStr;
use std::fs;
use std::path::PathBuf;

use super::EntityError;

/// A single card with two sides.
/// Both sides are markdown text, optional with latex math.
/// The [uuid](Techcard::uuid) contains the unique identifier of the
/// techcard.
/// The [name](Techcard::name) is the directory where the Card is located.
/// The [path](Techcard::path) is a hanlde to where the Card is stored on the
/// filesystem.
#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Techcard {
    pub(crate) uuid: Uuid,
    pub(crate) name: String,
    pub side_a: String,
    pub side_b: String,
    pub(crate) path: PathBuf,
}

impl Techcard {
    pub fn uuid(&self) -> &Uuid {
        &self.uuid
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn path(&self) -> &Path {
        &self.path
    }

    /// Parse a Techcard's filesystem representation at `dir`.
    pub fn parse_dir(dir: PathBuf) -> Result<Techcard, EntityError> {
        if !dir.is_dir() {
            return Err(EntityError::NoDir(dir));
        }

        let name = super::dir_name(&dir)?;

        let uuidname = OsStr::new("techcard");
        let sideaname = OsStr::new("side_a");
        let sidebname = OsStr::new("side_b");
        let files = vec![uuidname, sideaname, sidebname];

        let file_content = super::read_files(&dir, &files)?;

        let uuid = file_content.get(uuidname).ok_or(EntityError::FileMissing {
            part: uuidname.to_owned(),
            path: dir.to_owned(),
        })?;
        let side_a = file_content
            .get(sideaname)
            .ok_or(EntityError::FileMissing {
                part: sideaname.to_owned(),
                path: dir.to_owned(),
            })?;
        let side_b = file_content
            .get(sidebname)
            .ok_or(EntityError::FileMissing {
                part: sidebname.to_owned(),
                path: dir.to_owned(),
            })?;

        Ok(Techcard {
            uuid: Uuid::parse_str(uuid)?,
            name,
            side_a: side_a.to_string(),
            side_b: side_b.to_string(),
            path: dir,
        })
    }

    /// Recursively deletes a Techcard's filesystem representation at `path`.
    ///
    /// Does not do any safety checks.
    pub fn delete(self) -> Result<(), (Self, EntityError)> {
        fs::remove_dir_all(&self.path)
            .map_err(|e| EntityError::DirDelete {
                path: self.path.to_owned(),
                source: e,
            })
            .map_err(|e| (self, e))?;
        Ok(())
    }

    /// Creates a new in-memory representation of a Techcard.
    /// To save the representation call `save()`.
    ///
    /// Generates a new UUID for the Techcard on invocation.
    pub fn new(
        name: String,
        side_a: String,
        side_b: String,
        dir: PathBuf,
    ) -> Result<Techcard, EntityError> {
        if !dir.ends_with(&name) {
            return Err(EntityError::NameMissmatch { name, path: dir });
        }

        Ok(Techcard {
            uuid: Uuid::new_v4(),
            name,
            side_a,
            side_b,
            path: dir,
        })
    }

    /// Saves the Techcard's content at `path`.
    ///
    /// If the directory for the unit does not exist,
    /// it is created.
    /// Note that the parent directory must already exist though.
    pub fn save(self) -> Result<Self, (Self, EntityError)> {
        let mut files: HashMap<&OsStr, &[u8]> = HashMap::new();
        let uuid = self.uuid.to_string();
        files.insert(OsStr::new("techcard"), uuid.as_bytes());
        files.insert(OsStr::new("name"), self.name.as_bytes());
        files.insert(OsStr::new("side_a"), self.side_a.as_bytes());
        files.insert(OsStr::new("side_b"), self.side_b.as_bytes());

        let mut path = PathBuf::from(&self.path);

        match super::save_files(&mut path, files) {
            Ok(_) => Ok(self),
            Err(e) => Err((self, e)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn read() {
        let result = Techcard::parse_dir(PathBuf::from(r"./tests/single_unit/simple_card"));
        match result {
            Ok(card) => println!("{:?}", card),
            Err(error) => {
                println!("Error: {}", error);
                panic!()
            }
        }
    }
}
