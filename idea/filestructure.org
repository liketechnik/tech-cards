* File layout

** Techcards (well, basically Flashcards), organized in Units
** Both are directories, containing: marker file (.unit, .techcard), other units, techcards, (uu)id, side a/side b or description, addititional files
** root directory is unit
** content/description are markdown, first headline is title, directory name is name of techcard/unit
** title: shown when viewing a module/techcard; name: shown in overview
